extends Node

const URL := "https://hovertank-havok.herokuapp.com/v1/records"
const SSL := true

signal error
signal upload_complete
signal download_complete(records)

var upload_request := HTTPRequest.new()
var download_request := HTTPRequest.new()

func _ready():
	upload_request.timeout = 30
	download_request.timeout = 10
	add_child(upload_request)
	add_child(download_request)
	upload_request.connect("request_completed", self, "_on_upload_completed")
	download_request.connect("request_completed", self, "_on_download_completed")

func fail(msg: String):
	emit_signal("error", msg)
	push_error(msg)

func upload(name: String, score: int):
	var s := to_json({"name": name, "score": score})
	var err := upload_request.request(URL, [], SSL, HTTPClient.METHOD_POST, s)
	if err != OK:
		fail("Failed to make request: %d" % err)
	print_debug("Upload initiated")

func download():
	var err := download_request.request(URL, [], SSL, HTTPClient.METHOD_GET)
	if err != OK:
		fail("Failed to make request: %d" % err)
	print_debug("Download initiated")

func check_result(result: int, code: int) -> bool:
	if result != HTTPRequest.RESULT_SUCCESS:
		fail("HTTP result %d != %d" % [result, HTTPRequest.RESULT_SUCCESS])
		return false
	if code != 200:
		fail("HTTP response %d != %d" % [code, 200])
		return false
	return true

func _on_upload_completed(result: int, code: int, _headers: PoolStringArray, _body: PoolByteArray):
	check_result(result, code)
	print_debug("Upload complete")
	emit_signal("upload_complete")

func _on_download_completed(result: int, code: int, _headers: PoolStringArray, body: PoolByteArray):
	if !check_result(result, code):
		return

	var utf8 := body.get_string_from_utf8()
	var records = parse_json(utf8) as Array
	if records == null:
		fail("Bad data: %s" % utf8)
		return

	for r in records:
		var d := r as Dictionary
		if not d and d.has("name") and d.has("score"):
			fail("Bad record: %s" % r)
			continue

	emit_signal("download_complete", records)
	print_debug("Upload complete")
