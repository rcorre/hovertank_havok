extends Position3D

const RANGE := 200.0
const SPAWN_CHANCE := 0.2
const CRATE_CHANCE := 0.2
const CRATE_SCENE := preload("res://objects/crate/Crate.tscn")

onready var player: Spatial = get_tree().get_nodes_in_group("player")[0]

func _physics_process(_delta: float):
	if not is_instance_valid(player):
		return
	if player.global_transform.origin.distance_to(global_transform.origin) < RANGE:
		var scene: PackedScene = null
		if randf() < CRATE_CHANCE:
			scene = CRATE_SCENE
		elif randf() < SPAWN_CHANCE + Global.difficulty_factor:
			var r := randf()
			if r < 0.5:
				scene = preload("res://objects/tank/Tank.tscn")
			else:
				scene = preload("res://objects/turret/Turret.tscn")

		if scene:
			var obj := scene.instance() as Spatial
			get_parent().add_child(obj)
			obj.global_transform = global_transform
		queue_free()
