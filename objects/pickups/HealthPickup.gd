extends Area

const HEAL_AMOUNT := 50.0

func _ready():
	connect("body_entered", self, "_on_body_entered", [], CONNECT_ONESHOT)

func _on_body_entered(body: Node):
	body.propagate_call("heal", [HEAL_AMOUNT])
	hide()
