extends Area

const DATA := [
	{
		"scene": preload("res://objects/weapons/flak/Flak.tscn"),
		"name": "FLAK",
		"icon": preload("res://assets/icons/striking-balls.svg"),
		"sound": preload("res://assets/sounds/flak_voice.wav"),
	},
	{
		"scene": preload("res://objects/weapons/auto/Auto.tscn"),
		"name": "AUTO",
		"icon": preload("res://assets/icons/heavy-bullets.svg"),
		"sound": preload("res://assets/sounds/auto_voice.wav"),
	},
	{
		"scene": preload("res://objects/weapons/rail/Rail.tscn"),
		"name": "RAIL",
		"icon": preload("res://assets/icons/ringed-beam.svg"),
		"sound": preload("res://assets/sounds/rail_voice.wav"),
	},
]

onready var sprite: Sprite3D = $Sprite3D
onready var sound: AudioStreamPlayer = $Sound

var weapon_kind: int

func _ready():
	weapon_kind = randi() % len(DATA)
	var data = DATA[weapon_kind]
	sprite.set_texture(data.icon)
	sound.stream = data.sound
	connect("body_entered", self, "_on_body_entered", [], CONNECT_ONESHOT)

func _on_body_entered(body: Node):
	body.propagate_call("equip", [DATA[weapon_kind].scene])
	hide()
	sound.connect("finished", self, "queue_free")
	sound.play()
