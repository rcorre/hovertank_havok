extends Spatial

export(PackedScene) var weapon_pickup: PackedScene
export(PackedScene) var health_pickup: PackedScene

func _ready():
	var scene := weapon_pickup if randf() > 0.5 else health_pickup 
	add_child(scene.instance())
