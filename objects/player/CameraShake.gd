# inspired by http://kidscancode.org/godot_recipes/2d/screen_shake/
extends Camera
class_name CameraShake

const GROUP := "camera_shake"
const SHAKE_DECAY := 2.0  # How quickly the shaking stops
const MAX_SHAKE_OFFSET := Vector2(6, 3)  # Maximum hor/ver shake in pixels.
const SHAKE_POWER := 2  # shake_amount exponent. Use [2, 3].
const MAX_SHAKE := 1.5

onready var noise := OpenSimplexNoise.new()

var shake_amount := 0.0  # Current shake strength.
var noise_y := 0.0  # Current shake strength.

func _ready():
	randomize()
	noise.seed = randi()
	noise.period = 4
	noise.octaves = 2
	add_to_group(GROUP)

func _process(delta):
	shake_amount = max(shake_amount - SHAKE_DECAY * delta, 0)
	noise_y += 1
	h_offset = MAX_SHAKE_OFFSET.x * shake_amount * noise.get_noise_2d(noise.seed*2, noise_y)
	v_offset = MAX_SHAKE_OFFSET.y * shake_amount * noise.get_noise_2d(noise.seed*3, noise_y)

func shake(intensity: float):
	shake_amount = min(MAX_SHAKE, shake_amount + intensity / 100.0)
