extends Label

func _ready():
	Leaderboard.connect("error", self, "_on_scores_error")

func _on_scores_error(error: String):
	set_text(error)
	show()
