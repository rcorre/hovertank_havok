extends Button

func _pressed():
	if Global.player_name:
		Leaderboard.upload(Global.player_name, Global.total_score)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Global.restart_game()
