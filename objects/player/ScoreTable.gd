extends GridContainer

func add_label(text: String):
	var l := Label.new()
	l.set_text(text)
	add_child(l)

func _ready():
	Leaderboard.download()
	Leaderboard.connect("download_complete", self, "_on_scores_ready")

func _on_scores_ready(scores: Array):
	for i in range(len(scores)):
		if Global.total_score > scores[i].score:
			scores.insert(i, {"name": "", "score": Global.total_score})
			break

	for i in range(len(scores)):
		var entry = scores[i]

		add_label("%2d" % (i + 1))
		add_child(VSeparator.new())

		if entry.name:
			add_label(entry.name)
		else:
			var le := LineEdit.new()
			le.caret_blink = true
			le.max_length = 8
			le.placeholder_text = "NAME"
			le.text = Global.player_name
			le.caret_position = len(le.text)
			le.connect("text_changed", Global, "set_player_name")
			add_child(le)
			le.grab_focus()

		add_child(VSeparator.new())

		add_label("%d" % entry.score)

		for _i in range(columns):
			add_child(HSeparator.new())
