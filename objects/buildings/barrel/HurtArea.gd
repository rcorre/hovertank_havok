extends Area

var processed := false

export(float, 0, 1000) var damage := 100.0

func _physics_process(_delta: float):
	# skip one frame for collisions to shake out
	# this whole script is terrible but I'm rushing
	if not processed:
		processed = true
		return
	for b in get_overlapping_bodies():
		b.propagate_call("hurt", [damage])

	queue_free()
