extends Spatial

export(float, 0, 10000) var max_health := 100.0
export(PackedScene) var explosion_scene: PackedScene
export(Material) var hit_material: Material
export(int) var score := 10

var health := 0.0

func _ready():
	health = max_health

func hurt(damage: float):
	health -= damage
	if health <= 0:
		var explosion := explosion_scene.instance() as Spatial
		get_tree().current_scene.add_child(explosion)
		explosion.global_transform = global_transform
		queue_free()
		Global.add_score(score)
	else:
		# flash a different color
		propagate_call("set_material_override", [hit_material])
		get_tree().create_timer(0.1).connect("timeout", self, "propagate_call", ["set_material_override", [null]])
