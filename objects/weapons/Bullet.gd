extends KinematicBody

export(float, 0, 1000) var damage := 25.0
export(float, 0, 1000) var speed := 100.0
export(float, 0, 10) var duration := 3.0

func _physics_process(delta: float):
	var col := move_and_collide(global_transform.basis.z * speed * delta)
	if col:
		queue_free()
		(col.collider as Node).propagate_call("hurt", [damage])
