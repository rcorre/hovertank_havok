extends Spatial

export(PackedScene) var bullet: PackedScene
export(float, 1, 600) var rpm := 300.0
export(int, 1, 16) var shots := 1
export(float, 0, 60) var spread := 0.0

onready var fire_point: Spatial = $FirePoint
onready var fire_sound: AudioStreamPlayer3D = $FireSound

var cooldown := 0.0
var firing := false

func fire_primary(on: bool):
	firing = on

func _physics_process(delta: float):
	cooldown = max(0, cooldown - delta)
	if firing and cooldown <= 0.0:
		cooldown = 60.0 / rpm
		var angle_delta := 0.0 if shots == 1 else spread / (shots - 1)
		for i in range(shots):
			var b := bullet.instance() as Spatial
			get_tree().current_scene.add_child(b)
			b.global_transform = fire_point.global_transform
			b.rotate_y(deg2rad(angle_delta * i - spread / 2))
			fire_sound.play()
