extends Area

signal target_in_area(on)

onready var ray: RayCast = $RayCast

func _physics_process(_delta: float):
	for body in get_overlapping_bodies():
		ray.cast_to = ray.to_local(body.global_transform.origin)
		ray.force_raycast_update()
		if ray.get_collider() == body:
			emit_signal("target_in_area", true)
			return

	emit_signal("target_in_area", false)
