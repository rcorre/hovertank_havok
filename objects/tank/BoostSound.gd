extends AudioStreamPlayer

const MAX_SPEED:= 15.0

var vel := SpatialVelocityTracker.new()

func _physics_process(_delta: float):
	var p := get_parent() as Spatial
	vel.update_position(p.global_transform.origin)
	var factor := vel.get_tracked_linear_velocity().length() / MAX_SPEED
	pitch_scale = 0.8 + factor / 2.0
	volume_db = -15 + 5 * factor
