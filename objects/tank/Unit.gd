extends KinematicBody

signal health_changed(val)

export(float, 0, 10000) var max_health := 100
export(PackedScene) var explosion_scene: PackedScene
export(Material) var hit_material: Material
export(float, 0, 1000) var max_range := 100.0
export(float, 0, 100) var max_speed:= 15.0
export(float, 0, 100) var accel := 30.0
# Don't get closer than this to the player
export(float, 0, 100) var proximity_limit := 40.0
export(float, 0, 6.0) var turn_rate := 2.0
export(int) var score := 50

onready var target: Spatial = get_tree().get_nodes_in_group("player")[0]

var velocity := Vector3.ZERO
var look_target := 0.0
var health := 0.0

func _ready():
	health = max_health

func _physics_process(delta: float):
	if not is_instance_valid(target):
		return

	var target_pos := target.global_transform.origin
	var dist := target_pos.distance_to(global_transform.origin)
	if dist > max_range or dist < proximity_limit:
		return

	var target_vel := global_transform.origin.direction_to(target_pos) * max_speed
	velocity = velocity.move_toward(target_vel, delta * accel)
	velocity.y -= Global.GRAVITY * delta
	velocity = move_and_slide(velocity)

	var desired_facing := global_transform.looking_at(target_pos, Vector3.UP).basis.get_rotation_quat()
	global_transform.basis = global_transform.basis.get_rotation_quat().slerp(desired_facing, delta * turn_rate)

func hurt(damage: float):
	health -= damage
	emit_signal("health_changed", health)
	if health <= 0:
		var explosion := explosion_scene.instance() as Spatial
		get_tree().current_scene.add_child(explosion)
		explosion.global_transform = global_transform
		queue_free()
		Global.add_score(score)
	else:
		propagate_call("set_material_override", [hit_material])
		get_tree().create_timer(0.1).connect("timeout", self, "propagate_call", ["set_material_override", [null]])
