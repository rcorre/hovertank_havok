extends AnimationPlayer

func _ready():
	Global.connect("score_changed", self, "stop", [])
	Global.connect("score_changed", self, "play", ["score"])
