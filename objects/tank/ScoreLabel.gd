extends Label

func _ready():
	_on_score_changed()
	Global.connect("score_changed", self, "_on_score_changed")

func _on_score_changed():
	set_text("%6d" % Global.total_score)
