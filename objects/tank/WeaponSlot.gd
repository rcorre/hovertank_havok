extends Spatial

func equip_primary(weapon: PackedScene):
	for c in get_children():
		c.queue_free()
	call_deferred("add_child", weapon.instance())
