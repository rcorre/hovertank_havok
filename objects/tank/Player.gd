extends KinematicBody

const MOUSE_SENSITIVITY := 0.003
const MAX_SPEED:= 15.0
const ACCEL := 30.0
const FUEL_USE := 0.5

signal health_changed(val)
signal boost_on
signal boost_off

export(float, 0, 10000) var max_health := 100
export(PackedScene) var explosion_scene: PackedScene
export(Material) var hit_material: Material
export(PackedScene) var death_camera_scene: PackedScene

onready var turret: Spatial = $TurretPhysics

var velocity := Vector3.ZERO
var look_target := 0.0
var health := 0.0
var fuel := 1.0
var boosting := false

func _ready():
	health = max_health

func _input(ev: InputEvent):
	# TODO: do this once from main menu
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	var mouse := ev as InputEventMouseMotion
	if mouse:
		look_target -= mouse.relative.x * MOUSE_SENSITIVITY
	elif ev.is_action("Fire Primary"):
		propagate_call("fire_primary", [ev.is_pressed()])
	elif ev.is_action("Fire Secondary"):
		propagate_call("fire_secondary", [ev.is_pressed()])
	elif ev.is_action_pressed("boost"):
		emit_signal("boost_on")
		boosting = true
	elif ev.is_action_released("boost"):
		emit_signal("boost_off")
		boosting = false

func _physics_process(delta: float):
	var boost := 1.0
	if boosting:
		if fuel > 0.0:
			fuel -= delta * FUEL_USE
			boost = 2.0
		else:
			boosting = false
			emit_signal("boost_off")
	else:
		fuel = min(1.0, fuel + delta)
	var target_vel := Vector3(
		Input.get_action_strength("left") - Input.get_action_strength("right"),
		0,
		Input.get_action_strength("forward") - Input.get_action_strength("backward")
	).rotated(Vector3.UP, look_target) * MAX_SPEED * boost
	velocity.y -= Global.GRAVITY * delta
	velocity = move_and_slide(velocity.move_toward(target_vel, delta * ACCEL * boost))
	turret.rotation.y = lerp_angle(turret.rotation.y, look_target, 0.1)

func die():
	var explosion := explosion_scene.instance() as Spatial
	get_tree().current_scene.add_child(explosion)
	explosion.global_transform = global_transform
	queue_free()
	var cam := death_camera_scene.instance() as Spatial
	get_tree().current_scene.add_child(cam)
	cam.global_transform = global_transform
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func hurt(damage: float):
	health -= damage
	emit_signal("health_changed", health)
	if health <= 0:
		die()
	else:
		propagate_call("set_material_override", [hit_material])
		get_tree().create_timer(0.1).connect("timeout", self, "propagate_call", ["set_material_override", [null]])

func heal(amount: float):
	health = min(max_health, health + amount)
	emit_signal("health_changed", health)

func equip(weapon: PackedScene):
	propagate_call("equip_primary", [weapon])
