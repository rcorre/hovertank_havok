extends KinematicBody

signal damaged(amount)

func hurt(amount: float):
	emit_signal("damaged", amount)
