extends Spatial

export(float, 0, 1000) var max_range := 100.0
export(float, 0, 1000) var turn_rate := 3.0

onready var target: Spatial = get_tree().get_nodes_in_group("player")[0]

signal damaged(amount)

func hurt(amount: float):
	emit_signal("damaged", amount)

func _physics_process(delta: float):
	if not is_instance_valid(target):
		return

	var target_pos := target.global_transform.origin
	if target_pos.distance_to(global_transform.origin) > max_range:
		return

	var turn_to := global_transform.looking_at(target_pos, Vector3.UP).rotated(Vector3.UP, PI).basis
	global_transform.basis = global_transform.basis.slerp(turn_to, turn_rate * delta)
