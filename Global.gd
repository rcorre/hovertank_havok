extends Node

const GRAVITY := 9.8
const CONFIG_PATH := "user://settings.cfg"
const DIFFICULTY_PER_SECOND := 0.2 / 60.0

signal score_changed()

var total_score := 0
var player_name := ""
var difficulty_factor := 0.0

func _ready():
	var config := ConfigFile.new()
	if config.load(CONFIG_PATH) == OK:
		player_name = config.get_value("player", "name", "")
	else:
		printerr("Failed to load config")

func add_score(amount: int):
	total_score += amount
	emit_signal("score_changed")

func restart_game():
	total_score = 0
	difficulty_factor = 0
	if player_name != "":
		var config := ConfigFile.new()
		config.set_value("player", "name", player_name)
		config.save(CONFIG_PATH)

	get_tree().change_scene("res://scene/world/World.tscn")

func set_player_name(name: String):
	player_name = name

func _physics_process(delta: float):
	difficulty_factor += delta * DIFFICULTY_PER_SECOND
