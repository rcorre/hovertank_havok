extends Spatial

const TILE_SIZE := 100.0
const MAX_SIZE := 100.0

onready var player := get_tree().get_nodes_in_group("player")[0] as Spatial

export(PackedScene) var start_tile: PackedScene
export(Array, PackedScene) var tile_scenes: Array

var tiles := {}

func _ready():
	var t := start_tile.instance() as Spatial
	add_child(t)
	tiles[0] = {0: t}

func _physics_process(_delta: float):
	if not is_instance_valid(player):
		return
	var coord := player.global_transform.origin / TILE_SIZE
	var cur_x := int(coord.x)
	var cur_z := int(coord.z)

	for x in tiles:
		for z in tiles[x]:
			if abs(x - cur_x) > 2 or abs(z - cur_z) > 2:
				tiles[x][z].queue_free()
				tiles[x].erase(z)

	for x in range(cur_x - 2, cur_x + 3):
		for z in range(cur_z - 2, cur_z + 3):
			if not tiles.has(x):
				tiles[x] = {}
			if not tiles[x].has(z):
				var t := tile_scenes[randi() % len(tile_scenes)].instance() as Spatial
				add_child(t)
				t.transform.origin = Vector3(x, 0, z) * TILE_SIZE
				tiles[x][z] = t
