# Hovertank Havok

A game made for the [gotm.io jam #1](https://itch.io/jam/gotm-jam-1).

# Credits

- [FunkyFuture8 Palette](https://lospec.com/palette-list/funkyfuture-8) by [ShamaBoy](https://lospec.com/shamaboy11)
- Music by [Doctor Dreamchip](https://www.youtube.com/channel/UCbhlcItuC6pmhhemUjhPt1w) (CC0)
- My voice processed in [Audacity](https://www.audacityteam.org/)
- Sounds created in [jsfxr](http://sfxr.me/)
- Game made in [Godot](https://godotengine.org/)
- Models made in [Blender](https://www.blender.org/)
