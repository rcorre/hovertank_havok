extends RayCast

export(float, 0, 1000) var damage := 50.0

onready var mesh: MeshInstance = $MeshInstance 

func _physics_process(_delta: float):
	if is_colliding():
		(get_collider() as Node).propagate_call("hurt", [damage])
		var dist := global_transform.origin.distance_to(get_collision_point())
		mesh.scale.z = dist / cast_to.z

	# don't hit again
	enabled = false

	# Show our beam for a bit, then disappear
	get_tree().create_timer(0.2).connect("timeout", self, "queue_free")
